from django.db import models

from django.contrib.auth.models import User 
from allauth.account.signals import user_signed_up 
from django.dispatch import receiver 

estados = (
        ('D', 'Disponible'),
        ('R', 'Rescatado'),
        ('A', 'Adoptado'),)
class Perro(models.Model):
    titulo = models.CharField(max_length=100, verbose_name=u'Nombre Perro', unique=True)
    Raza = models.CharField(max_length=100, verbose_name=u'Raza')
    Descripcion = models.TextField(verbose_name=u'Descripcion', help_text=u'Ingresa tu descripcion')
    imagen = models.ImageField(upload_to='media', verbose_name=u'Imágen', blank=True, null=True)
    tiempo_registro = models.DateTimeField(auto_now=True)
    usuario = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    estados = models.CharField(max_length=1,choices=estados)
    def __unicode__(self):
        return self.titulo

