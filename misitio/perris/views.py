from django.shortcuts import render
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm, UserChangeForm
from django.contrib.auth.models import User
from django.core.mail import EmailMessage
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import (get_object_or_404, redirect, render, render_to_response)
from .models import Perro 
from .forms import PerroForm
from django.contrib import auth, messages
from django.core.mail import EmailMessage
from django.core.mail import send_mail
from django.conf import settings
from django.core import serializers
import json

def usuario_nuevo(request):
    if request.method=='POST':
        formulario = UserCreationForm(request.POST)
        if formulario.is_valid:
            formulario.save()
            return HttpResponseRedirect('/')
    else:
        formulario = UserCreationForm()
    context = {'formulario': formulario}
    return render(request, 'nuevouser.html', context)

  
def list(request):
    return render(request, 'index.html', {})

def ingresar(request):
    
    if request.method == 'POST':
        formulario = AuthenticationForm(request.POST)
        if formulario.is_valid:
            usuario = request.POST['username']
            clave = request.POST['password']
            acceso = authenticate(username=usuario, password=clave)
            if acceso is not None:
                if acceso.is_active:
                    login(request, acceso)
                    return HttpResponseRedirect('/cliente/privado')
                else:
                    return render(request, 'no_activo_user.html')
            else:
                return render(request, 'no_existe_user.html')
    else:
        formulario = AuthenticationForm()
    context = {'formulario': formulario}
    return render(request, 'login.html', context)

@login_required(login_url='/cliente/ingresar')
def privado(request):
    if request.user.is_staff:
        usuario = request.user
        context = {'usuario': usuario}
        return render(request, 'admin.html', context)
    if request.user.is_staff == False:
        usuario = request.user
        context = {'usuario': usuario}
        return render(request, 'user.html', context)


def Perro_nuevo(request):
    if request.method=='POST':
        formulario = PerroForm(request.POST, request.FILES)
        if formulario.is_valid():
            formulario.save()
            malito = email(request)
            return HttpResponseRedirect('/')
    else:
        formulario = PerroForm()
    context = {'formulario': formulario}
    return render(request, 'nuevoperro.html', context)


def lista_perros(request):
    perros = Perro.objects.all()
    return render(request, 'listarperro.html', {'datos': perros})



def eliminar_perros(request, id):
    if request.user.is_staff:
       perri = Perro.objects.get(id=id)
    try:
        perri.delete()
        mensaje = 'Perro Eliminado Existosamente.!!'
        messages.success(request, mensaje)
    except:
        mensaje = 'NO Eliminado Exitosamente..!!'
        messages.error(request, mensaje)
        if request.user.is_staff == False:
            mensaje = 'NO PUEDES ELEMINAR PERROS, SOLO EL ADMINISTRADOR'
            messages.error(request, mensaje)
    return render(request,'listarperro.html')


#MODIFICAR (INCOMPLETO)
def modificar_perro(request, id):
    perri = Perro.objects.get(id=id)
    perru =  Perro.objects.all()
    variable = {
        'perri': perri,
        'perru': perru
    }
    if request.method=='POST':

        perri = Perro()
        perri.id = request.POST.get('txtid')
        perri.Raza = request.POST.get('txtRaza')
        perri.titulo = request.POST.get('txttitulo')
        perri.Descripcion = request.POST.get('txtdescripcion')
        perri.estados = request.POST.get('cbestado')
        perri.usuario = request.POST.get('txtusuario')
        perri.imagen = request.POST.get('cbimg')
        perri.save()

        return HttpResponseRedirect('/')
    else:
         return render(request, 'modificar_perros.html', variable)



#ENVIO DE MAILS
def email(request):
    subject = 'PERROS DISPONIBLES (www.misperris.com)'
    message = ' Tenemos nuevas mascotas para adoptar, ingresa a nuestro sitio www.MisPerris.com  TE ESPERAMOS '
    email_from = settings.EMAIL_HOST_USER
    recipient_list = ['begizaba@daabox.com',]
    send_mail( subject, message, email_from, recipient_list )
    return render(request,'index.html')

####



