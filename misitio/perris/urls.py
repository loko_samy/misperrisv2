from django.urls import path, re_path
from . import views
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import include, url

urlpatterns = [
    
    path('', views.list, name='list'),
    url(r'^cliente/registrar/$', views.usuario_nuevo, name='usuario_nuevo'),
    url(r'^cliente/ingresar/$', views.ingresar, name='ingresar'),
    url(r'^cliente/privado/$', views.privado, name='privado'),
    url(r'^perro/nuevo/$', views.Perro_nuevo, name='perro_nuevo'),
    url(r'^perro/lista/$', views.lista_perros, name='lista_perros'),
    path(r'^perro/eliminar/<id>/', views.eliminar_perros, name='eliminar_perros'),
    path(r'^perro/modificar/<id>/', views.modificar_perro, name='modificar_perros'),
    url(r'^accounts/', include('allauth.urls')),

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns +=static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
