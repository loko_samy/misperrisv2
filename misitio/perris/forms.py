from django.forms import ModelForm 
from django import forms
from .models import Perro, User
from django.contrib.auth.forms import UserChangeForm


class PerroForm(ModelForm):
    class Meta:
        model = Perro
        fields = '__all__'


