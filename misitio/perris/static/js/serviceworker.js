// name and version of cache
const CACHE_NAME = 'misperris';

// for cache

var urlsToCache = [
    './',
    './css/styles.css',
    './img/favicon.png',
    './img/1.jpg',
    './img/2.jpg',
    './img/3.jpg',
    './img/4.jpg',
    './img/5.jpg',
    './img/6.png',
    './img/favicon-1024.png',
    './img/favicon-512.png',
    './img/favicon-384.png',
    './img/favicon-256.png',
    './img/favicon-192.png',
    './img/favicon-128.png',
    './img/favicon-96.png',
    './img/favicon-64.png',
    './img/favicon-32.png',
    './img/favicon-16.png'
];

// install event

self.addEventListener('install', e => {
    e.waitUntil(
        caches.open(CACHE_NAME)
                .then(cache => {
                    return cache.addAll(urlsToCache)
                                .then(() =>{
                                    self.skipWaiting();
                                });

                })
                .catch(err => {
                    console.log('No se ha registrado el cache', err);
                })
    );
});

// activate event

self.addEventListener('activate', e => {
    const cacheWhiteList = [CACHE_NAME];

    e.waitUntil(
        caches.keys()
                .then(cacheNames => {
                    return Promise.all(
                        cacheNames.map(cacheName => {

                            if(cacheWhiteList.indexOf(cacheName) === -1){
                                // Borrar elementos que no se necesitan
                                return caches.delete(cacheName);

                            }
                        })

                    );
                })
                .then(() => {
                    //Activar cache
                    self.clients.claim();
                })

    );
});


// fetch event

self.addEventListener('fetch', function(event) {
    event.respondWith(
     caches.match(event.request).then(function(response) {
        return response || fetch(event.request);
     })
    );
 });

